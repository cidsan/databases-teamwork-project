# Databases Project - Web Application

Team Name: SmartOne

Team Leader: [Ventsislav Chernev](https://gitlab.com/vpchernev), [Stiliyan Kostadinov](https://gitlab.com/cidsan)

Team Members: [Ventsislav Chernev](https://gitlab.com/vpchernev), [Stiliyan Kostadinov](https://gitlab.com/cidsan)

Application info: This application is a database of movies, which provides the normal user with variety of options to browse in our database, full of movies, actors, directors and producers. It allows the admin to upload, edit and remove existing entities.

Features:

**Admin**

_**C**reate_

*Add Actor*: - Adds a new actor to the database.
*Add Director*: - Adds a new director to the database.
*Add Movie*: - Adds a movie to the database.

_**U**pdate_

*Edit Actor*: - Edits already existing actor.
*Edit Movie*: - Edits already existing movie.

_**D**elete_

*Remove Actor*: - Removes an actor from the database.
*Remove Movie*: - Removes a movie from the database.

**User and Admin**

_**R**ead_

You can list all movies and browse their details.
You can list all actors and browse their details.
You can list all directors and browse their details.

You can filter the entities by first letter and/or any letter/word contained in their names.

# Side information

**Name of application :** YouWatch

**Feature Owner :** Ventsislav Chernev, Stiliyan Kostadinov

Trello board: [Link](https://trello.com/b/GXeQvawc/youwatch-aspnet)


If you have any questions or ideas for new functionalities or developing the existing ones, you may contact the developers:

Ventsislav Chernev - vpchernevvv@gmail.com

Stiliyan Kostadinov - st.k.kostadinov@gmail.com


