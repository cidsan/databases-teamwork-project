﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.ActorServiceTest
{
    [TestClass]
    public class AddMovieAsync_Should
    {
        static Director testDirector = new Director()
        {
            FirstName = "Ivan",
            LastName = "Ivanov",
            Oscars = 3,
            ImageUrl = "https://www.facebook.com"
        };

        static Actor testActor = new Actor()
        {
            FirstName = "Gancho",
            LastName = "Penchev",
            Oscars = 1,
            ImageUrl = "https://www.google.com/"
        };

        static Movie testMovie = new Movie()
        {
            Name="Matrix",
            Oscars=3,
            Director=testDirector
        };

        [TestMethod]
        public async Task Throws_ActorNotFoundException_When_Passed_Id_Is_Not_Found_In_Database()
        {
            var options = TestUtils.GetOptions(nameof(Throws_ActorNotFoundException_When_Passed_Id_Is_Not_Found_In_Database));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                await Assert.ThrowsExceptionAsync<ActorNotFoundException>(async () => await sut.AddMovieAsync(1, 1));
            }
        }

        [TestMethod]
        public async Task Throws_MovieNotFoundException_When_Passed_Id_Is_Not_Found_In_Database()
        {
            var options = TestUtils.GetOptions(nameof(Throws_MovieNotFoundException_When_Passed_Id_Is_Not_Found_In_Database));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                await arrangeContext.Actors.AddAsync(testActor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.AddMovieAsync(testActor.Id, 1));
            }
        }

        [TestMethod]
        public async Task Correctly_Assigns_Passed_Movie_To_Actor()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Assigns_Passed_Movie_To_Actor));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                await arrangeContext.Actors.AddAsync(testActor);
                await arrangeContext.Movies.AddAsync(testMovie);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                await sut.AddMovieAsync(testActor.Id, testMovie.Id);

                var getActorById = await assertContext.Actors.FirstAsync(x => x.FirstName == testActor.FirstName);
                var getMovieById = await assertContext.Movies.FirstAsync(x => x.Name == testMovie.Name);

                var actorMovie = getActorById.ActorMovie.First();


                Assert.AreEqual(actorMovie.Movie.Id, getMovieById.Id);
            }
        }
    }
}
