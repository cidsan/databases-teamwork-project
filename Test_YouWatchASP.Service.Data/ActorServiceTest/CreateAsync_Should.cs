﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.DataTest
{
    [TestClass]
    public class CreateAsync_Should
    {
        static Actor testActor = new Actor()
        {
            FirstName = "Gancho",
            LastName = "Penchev",
            Oscars = 1
        };

        static string firstName = "Gancho";
        static string lastName = "Penchev";

        [TestMethod]
        public async Task Throw_ArgumentNullException_When_Passed_FirstName_Is_Null()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ArgumentNullException_When_Passed_FirstName_Is_Null));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                string expectedMessage = "First name cannot be empty.";
                var message = await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.CreateAsync(null, lastName, 3, "https://www.google.com/"));

                Assert.AreEqual(expectedMessage, message.ParamName);
            }

        }

        [TestMethod]
        public async Task Throw_ArgumentNullException_When_Passed_LastName_Is_Null()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ArgumentNullException_When_Passed_LastName_Is_Null));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                string expectedMessage = "Last name cannot be empty.";
                var message = await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.CreateAsync(firstName, null, 3, "https://www.google.com/"));

                Assert.AreEqual(expectedMessage, message.ParamName);
            }

        }

        [TestMethod]
        public async Task Throw_ArgumentOutOfRangeException_When_Passed_Oscars_Are_Negative()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ArgumentOutOfRangeException_When_Passed_Oscars_Are_Negative));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                string expectedMessage = "Oscars cannot be negative.";
                var message = await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(async () => await sut.CreateAsync(firstName, lastName, -3, "https://www.google.com/"));

                Assert.AreEqual(expectedMessage, message.ParamName);
            }

        }

        [TestMethod]
        public async Task Throw_ArgumentException_When_Passed_Actor_Already_Exists()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ArgumentException_When_Passed_Actor_Already_Exists));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(arrangeContext);
                await sut.CreateAsync(firstName, lastName, 3, "https://www.google.com/");
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                string expectedMessage = "Error, actor with the same names already exists.";
                var message = await Assert.ThrowsExceptionAsync<ArgumentException>(async () => await sut.CreateAsync(firstName, lastName, 3, "https://www.google.com/"));

                Assert.AreEqual(expectedMessage, message.Message);
            }

        }

        [TestMethod]
        public async Task Correctly_Assign_FirstName_When_Valid_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Assign_FirstName_When_Valid_Data_Is_Passed));

            using (var actContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(actContext);

                await sut.CreateAsync(firstName, lastName, 1, "https://www.google.com/");

            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                Assert.AreEqual(assertContext.Actors.First().FirstName, "Gancho");

            }
        }

        [TestMethod]
        public async Task Correctly_Assign_LastName_When_Valid_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Assign_LastName_When_Valid_Data_Is_Passed));

            using (var actContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(actContext);

                await sut.CreateAsync(firstName, lastName, 1, "https://www.google.com/");

            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                Assert.AreEqual(assertContext.Actors.First().LastName, "Penchev");

            }
        }

        [TestMethod]
        public async Task Correctly_Assign_Oscars_When_Valid_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Assign_Oscars_When_Valid_Data_Is_Passed));

            using (var actContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(actContext);

                await sut.CreateAsync(firstName, lastName, 1, "https://www.google.com/");

            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                Assert.AreEqual(assertContext.Actors.First().Oscars, 1);
            }
        }

        [TestMethod]
        public async Task Correctly_Assign_ImageUrl_When_Valid_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Assign_ImageUrl_When_Valid_Data_Is_Passed));

            using (var actContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(actContext);

                await sut.CreateAsync(firstName, lastName, 1, "https://www.google.com/");

            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                Assert.AreEqual(assertContext.Actors.First().ImageUrl, "https://www.google.com/");
            }
        }

        [TestMethod]
        public async Task Add_Actor_To_Database_When_Correct_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Add_Actor_To_Database_When_Correct_Data_Is_Passed));

            using (var actContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(actContext);

                await sut.CreateAsync(firstName, lastName, 1, "https://www.google.com/");

            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                Assert.AreEqual(assertContext.Actors.Count(), 1);
            }
        }


    }
}
