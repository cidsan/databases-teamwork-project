﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.ActorServiceTest
{
    [TestClass]
    public class EditAsync_Should
    {
        static Actor testActor = new Actor()
        {
            FirstName = "Gancho",
            LastName = "Penchev",
            Oscars = 1,
            ImageUrl= "https://www.google.com/"

        };

        static string firstName = "Gancho";
        static string lastName = "Penchev";
        static int oscars = 3;
        static string imageUrl = "https://www.google.com/";

        [TestMethod]
        public async Task Throw_ActorNotFoundException_When_Passed_Id_Is_Not_Found_In_Database()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ActorNotFoundException_When_Passed_Id_Is_Not_Found_In_Database));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                await Assert.ThrowsExceptionAsync<ActorNotFoundException>(async () => await sut.EditAsync(1,firstName, lastName, oscars, imageUrl));
            }

        }

        [TestMethod]
        public async Task Updates_Details_Of_Actor_Correctly()
        {
            var options = TestUtils.GetOptions(nameof(Updates_Details_Of_Actor_Correctly));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                await arrangeContext.Actors.AddAsync(testActor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                var date = (await assertContext.Actors.FirstAsync(x => x.Id == testActor.Id)).ModifiedOn;
                await sut.EditAsync(testActor.Id, "Pesho", "Peshev", 8, "http://youtube.com");

                var actor = assertContext.Actors.First(x => x.Id == testActor.Id);


                Assert.AreEqual("Pesho", actor.FirstName);
                Assert.AreEqual("Peshev", actor.LastName);
                Assert.AreEqual(8, actor.Oscars);
                Assert.AreEqual("http://youtube.com", actor.ImageUrl);
                Assert.AreNotEqual(date, actor.ModifiedOn);
            }

        }
    }
}
