﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.ActorServiceTest
{
    [TestClass]
    public class GetActorAsync_Should
    {
        [TestMethod]
        public async Task Throw_ActorNotFoundException_When_Passed_Actor_Doesnt_Exist()
        {
            var options = TestUtils.GetOptions(nameof(Throw_ActorNotFoundException_When_Passed_Actor_Doesnt_Exist));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                await Assert.ThrowsExceptionAsync<ActorNotFoundException>(async () => await sut.GetActorAsync(1));
            }
        }

        [TestMethod]
        public async Task Return_Right_Actor_When_Valid_Data_Is_Passed()
        {
            var options = TestUtils.GetOptions(nameof(Return_Right_Actor_When_Valid_Data_Is_Passed));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var testActor = new Actor()
                {
                    FirstName="Gosho",
                    LastName="Goshev",
                    Oscars=10
                };

                await arrangeContext.Actors.AddAsync(testActor);
                await arrangeContext.SaveChangesAsync();

            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                var getActorById = await assertContext.Actors.FirstAsync(x => x.FirstName == "Gosho" && x.LastName == "Goshev");
                var actor = await sut.GetActorAsync(getActorById.Id);

                Assert.AreEqual(getActorById.Id, actor.Id);
            }
        }
    }
}
