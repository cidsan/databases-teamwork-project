﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.ActorServiceTest
{
    [TestClass]
    public class GetListOfActorsAsync_Should
    {
        [TestMethod]
        public async Task Correctly_Return_List_Of_Actors()
        {
            var options = TestUtils.GetOptions(nameof(Correctly_Return_List_Of_Actors));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var testActor = new Actor()
                {
                    FirstName = "Gosho",
                    LastName = "Goshev",
                    Oscars = 10
                };
                await arrangeContext.Actors.AddAsync(testActor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                var listOfActors = await sut.GetListOfActorsAsync();

                Assert.AreEqual(1, listOfActors.Count);
                Assert.AreEqual("Gosho", listOfActors.First().FirstName);
            }
        }
    }
}
