﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.ActorServiceTest
{
    [TestClass]
    public class RemoveAsync_Should
    {
        [TestMethod]
        public async Task Throws_ActorNotFoundException_When_Passed_Actor_Does_Not_Exist()
        {
            var options = TestUtils.GetOptions(nameof(Throws_ActorNotFoundException_When_Passed_Actor_Does_Not_Exist));

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                string expectedMessage = "Actor with Id: 3 was not found.";
                var message = await Assert.ThrowsExceptionAsync<ActorNotFoundException>(async () => await sut.RemoveAsync(3));

                Assert.AreEqual(expectedMessage, message.Message);
            }
        }

        [TestMethod]
        public async Task Successfully_Remove_Actor_From_Database()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Remove_Actor_From_Database));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(arrangeContext);
                var testActor = new Actor()
                {
                    FirstName = "Gosho",
                    LastName = "Goshev",
                    Oscars = 3,
                    CreatedOn = DateTime.Now
                };

                await arrangeContext.Actors.AddAsync(testActor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new ActorService(assertContext);
                var getActorById = await assertContext.Actors.FirstAsync(x => x.FirstName == "Gosho" && x.LastName == "Goshev");
                await sut.RemoveAsync(getActorById.Id);
                var actor = await assertContext.Actors
                            .FirstOrDefaultAsync(x => x.FirstName == "Gosho" && x.LastName == "Goshev");

                Assert.AreEqual(null, actor);
            }
        }


    }
}
