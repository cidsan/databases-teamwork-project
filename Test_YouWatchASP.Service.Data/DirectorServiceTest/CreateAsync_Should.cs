﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Datas
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public void Succed_When_Create_Director()
        {

            using (var contex = new ApplicationDbContext(TestUtils.GetOptions(nameof(Succed_When_Create_Director))))
            {
                var sut = new DirectorService(contex);

                var result = sut.CreateAsync("Sedef", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

                Assert.AreEqual(result.Result.FirstName, "Sedef");
                Assert.AreEqual(result.Result.LastName, "Marinchov");
                Assert.AreEqual(result.Result.Oscars, 3);
                Assert.AreEqual(result.Result.ImageUrl, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

            }

        }
        [TestMethod]
        public void Throw_Argument_Exeption_Message_When_Create_Director_With_Null_FirstName()
        {
            using (var contex = new ApplicationDbContext(TestUtils.GetOptions(nameof(Throw_Argument_Exeption_Message_When_Create_Director_With_Null_FirstName))))
            {
                var sut = new DirectorService(contex);

                var message = sut.CreateAsync(null, "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

                Assert.IsTrue(message.Exception.Message.Contains("First name cannot be empty."));
              
            }

        }
        [TestMethod]
        public void Throw_Argument_Exeption_Message_When_Create_Director_But_He_Already_Exist_In_Database()
        {
            var options = TestUtils.GetOptions(nameof(Throw_Argument_Exeption_Message_When_Create_Director_But_He_Already_Exist_In_Database));

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(context);

                var addFirst = sut.CreateAsync("Sedef", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

                
            }

            using (var assertContext = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(assertContext);

                var addSecond = sut.CreateAsync("Sedef", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

                Assert.IsTrue(addSecond.Exception.Message.Contains("Error, director with the same names already exists."));
            }

        }

        [TestMethod]
        public void Succesffully_Adding_Director_To_DataBase()
        {
            var options = TestUtils.GetOptions(nameof(Succesffully_Adding_Director_To_DataBase));

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(context);

                var addFirst = sut.CreateAsync("Sedef", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");


            };

            using (var assertContext = new ApplicationDbContext(options))
            {
                Assert.IsTrue(assertContext.Directors.Count() == 1);
            }

        }

    }
}