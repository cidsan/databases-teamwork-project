﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.DirectorServiceTest
{
    [TestClass]
    public class GetById_Should
    {
        static int id;

        static DateTime date = DateTime.Now;

        static Director director = new Director()
        {
            FirstName = "Peruan",
            LastName = "Kitaichov",
            Oscars = 3,
            ImageUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw"
        };

        [TestMethod]
        public void Successfully_Return_Director_If_Exist()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Return_Director_If_Exist));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Add(director);

                arrangeContext.SaveChangesAsync();

                id = director.Id;
            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(actAssertContext);

                var result = sut.GetById(id);

                Assert.AreEqual(result.Result.FirstName, "Peruan");
                Assert.AreEqual(result.Result.LastName, "Kitaichov");

            }
        }

        [TestMethod]
        public void Throw_Argument_Exeption_When_Directors_Id_Is_Not_Found()
        {
            var options = TestUtils.GetOptions(nameof(Throw_Argument_Exeption_When_Directors_Id_Is_Not_Found));

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(actAssertContext);

                var result = sut.GetById(1);

                Assert.IsTrue(result.Exception.Message.Contains(
                           "The requested director doesn't exist in the database."));
            }
        }
    }
}
