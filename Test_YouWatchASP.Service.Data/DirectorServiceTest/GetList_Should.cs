﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.DirectorServiceTest
{
    [TestClass]
    public class GetList_Should
    {
        [TestMethod]
        public void Successfully_Return_List_Of_Directors()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Return_List_Of_Directors));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(arrangeActContext);

                var firstDirector =  sut.CreateAsync
                                  ("Sedef", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

                var secondDirector = sut.CreateAsync
                                  ("Ahmed", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");

                var ThirdDirector = sut.CreateAsync
                                  ("Boncho", "Marinchov", 3, "https://www.youtube.com/watch?v=nlTuiLS9NKw");
            }

            using (var asserActContext = new ApplicationDbContext(options))
            {
                var sut = new DirectorService(asserActContext);

                var result = sut.GetListAsync();

                Assert.AreEqual(result.Result.Count(), 3);

            }
        }

    }
}
