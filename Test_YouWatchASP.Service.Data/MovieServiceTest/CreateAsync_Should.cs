﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data;
using YouWatchASP.Services.Data.Contracts;

namespace Test_YouWatchASP.Service.Data.MovieServiceTest
{
    [TestClass]
    public class CreateAsync_Should
    {
        static DateTime date = DateTime.Now;

        static Director director = new Director()
        {
            FirstName = "Peruan",
            LastName = "Kitaichov",
            Oscars = 3,
            ImageUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw"
        };

        static Movie firstMovie = new Movie()
        {
            Name = "Movietooo",
            Director = director,
            Oscars = 3,
            TrailerUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw",
            ImageUrl = "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743",
            Description = "Description",
            ReleaseDate = DateTime.Now
        };

        [TestMethod]
        public void Successfully_Create_Movie()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Create_Movie));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.Name, "Sea");
                Assert.AreEqual(movie.Result.Director.FirstName, "Peruan");
                Assert.AreEqual(movie.Result.Director.LastName, "Kitaichov");
                Assert.AreEqual(movie.Result.TrailerUrl, "https://www.youtube.com/watch?v=TcMBFSGVi1c");
                Assert.AreEqual(movie.Result.ImageUrl, "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743");
                Assert.AreEqual(movie.Result.Description, "Description");
                Assert.AreEqual(movie.Result.ReleaseDate, date);

            }

        }

        [TestMethod]
        public void Throw_Argument_Exeption_When_Movie_Name_Exist()
        {
            var options = TestUtils.GetOptions(nameof(Throw_Argument_Exeption_When_Movie_Name_Exist));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Directors.AddAsync(director);
                arrangeContext.Movies.Add(firstMovie);
                arrangeContext.SaveChangesAsync();
            }

            using (var actAssert = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssert);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync("Movietooo", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date));
            }
        }

        [TestMethod]
        public void Throw_Argument_Exeption_When_Director_Name_Dont_Exist()
        {
            var options = TestUtils.GetOptions(nameof(Throw_Argument_Exeption_When_Director_Name_Dont_Exist));


            using (var actAssert = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssert);

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync("Movietooo", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date));
            }
        }

        [TestMethod]
        public void Throw_Argument_Exeption_When_Director_Name_Is_In_Wrong_Format()
        {
            var options = TestUtils.GetOptions(nameof(Throw_Argument_Exeption_When_Director_Name_Is_In_Wrong_Format));


            using (var actAssert = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssert);

             var result =   Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync("Movietooo", "Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date));
            }
        }

        [TestMethod]
        public void Successfully_Add_Movie_Name()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Movie_Name));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.Name, "Sea");

            }

        }

        [TestMethod]
        public async Task Successfully_Add_Director_First_Name()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Director_First_Name));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                await arrangeActContext.Directors.AddAsync(director);
                await arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = await sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Director.FirstName, "Peruan");

            }

        }

        [TestMethod]
        public void Successfully_Add_Director_Last_Name()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Director_Last_Name));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.Director.LastName, "Kitaichov");

            }

        }

        [TestMethod]
        public void Successfully_Add_Movie_Oscars()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Movie_Oscars));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.Oscars, 2);

            }

        }

        [TestMethod]
        public void Successfully_Add_Movie_TrailerUrl()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Movie_TrailerUrl));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.TrailerUrl, "https://www.youtube.com/watch?v=TcMBFSGVi1c");

            }

        }

        [TestMethod]
        public void Successfully_Add_Movie_ImagerUrl()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Movie_ImagerUrl));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.ImageUrl, "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743");

            }

        }

        [TestMethod]
        public void Successfully_Add_Movie_Description()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Movie_Description));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.Description, "Description");

            }

        }

        [TestMethod]
        public void Successfully_Add_Movie_ReleaseDate()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Add_Movie_ReleaseDate));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                arrangeActContext.Directors.AddAsync(director);
                arrangeActContext.SaveChangesAsync();

            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var movie = sut.CreateAsync("Sea", "Peruan Kitaichov", 2, "https://www.youtube.com/watch?v=TcMBFSGVi1c", "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743", "Description", date);

                Assert.AreEqual(movie.Result.ReleaseDate, date);

            }

        }
    }
}
