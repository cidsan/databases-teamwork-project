﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.MovieServiceTest
{
    [TestClass]
    public class FindMovieDirectorById_Should
    {
        static DateTime date = DateTime.Now;

        static int id;

        static Director director = new Director()
        {
            FirstName = "Peruan",
            LastName = "Kitaichov",
            Oscars = 3,
            ImageUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw"
        };

        static Movie firstMovie = new Movie()
        {
            Name = "Movietooo",
            Director = director,
            Oscars = 3,
            TrailerUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw",
            ImageUrl = "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743",
            Description = "Description",
            ReleaseDate = DateTime.Now
        };

        [TestMethod]
        public void Successfully_Return_MovieDirector_If_Exist()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Return_MovieDirector_If_Exist));

            using (var arrangeContext = new ApplicationDbContext(options))
            {
                arrangeContext.Directors.Add(director);
                arrangeContext.Movies.Add(firstMovie);

                arrangeContext.SaveChangesAsync();

                id = firstMovie.Id;
            }

            using (var actAssertContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(actAssertContext);

                var result = sut.GetMovieById(id);

                Assert.AreEqual(result.Result.Name, "Movietooo");
                Assert.AreEqual(result.Result.Director.FirstName, "Peruan");
                Assert.AreEqual(result.Result.Director.LastName, "Kitaichov");


            }
        }
    }
}
