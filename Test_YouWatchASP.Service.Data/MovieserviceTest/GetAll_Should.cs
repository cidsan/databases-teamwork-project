﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Test_YouWatchASP.Service.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data;

namespace Test_YouWatchASP.Service.Data.MovieserviceTest
{
    [TestClass]
    public class GetAll_Should
    {
        [TestMethod]
        public void Successfully_Return_List_Of_Movies()
        {
            var options = TestUtils.GetOptions(nameof(Successfully_Return_List_Of_Movies));

            using (var arrangeActContext = new ApplicationDbContext(options))
            {

                var director = new Director()
                {
                    FirstName = "Peruan",
                    LastName = "Kitaichov",
                    Oscars = 3,
                    ImageUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw"
                };

                arrangeActContext.Directors.Add(director);

                var firstMovie = new Movie()
                {
                    Name = "Movietooo",
                    Director = director,
                    Oscars = 3,
                    TrailerUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw",
                    ImageUrl = "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743",
                    Description = "Description",
                    ReleaseDate = DateTime.Now
                };
                arrangeActContext.Movies.Add(firstMovie);

                var seconMovie = new Movie()
                {
                    Name = "Movietooo2",
                    Director = director,
                    Oscars = 3,
                    TrailerUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw",
                    ImageUrl = "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743",
                    Description = "Description",
                    ReleaseDate = DateTime.Now
                };
                arrangeActContext.Movies.Add(seconMovie);

                var thirdMoview = new Movie()
                {
                    Name = "Movietooo3",
                    Director = director,
                    Oscars = 3,
                    TrailerUrl = "https://www.youtube.com/watch?v=nlTuiLS9NKw",
                    ImageUrl = "https://webnews.bg/uploads/images/82/7382/417382/768x432.jpg?_=1554905743",
                    Description = "Description",
                    ReleaseDate = DateTime.Now
                };
                arrangeActContext.Movies.Add(thirdMoview);

                arrangeActContext.SaveChangesAsync();
            }

            using (var asserActContext = new ApplicationDbContext(options))
            {
                var sut = new MovieService(asserActContext);

                var result = sut.GetAllMovie();

                Assert.AreEqual(result.Result.Count(), 3);

            }

        }

        [TestMethod]
        public void Throw_Argument_Exeption_When_Movie_List_Is_Empty()
        {
            var options = TestUtils.GetOptions(nameof(Throw_Argument_Exeption_When_Movie_List_Is_Empty));

            using (var context = new ApplicationDbContext(options))
            {
                var sut = new MovieService(context);

                var result = sut.GetAllMovie();

                Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllMovie());

            }
        }
    }
}
