﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using YouWatchASP.Data.Context;

namespace Test_YouWatchASP.Service.Data
{
    public static class TestUtils
    {
        public static DbContextOptions<ApplicationDbContext> GetOptions(string databaseName)
        {
            //var provider = new ServiceCollection()
            //                 .AddEntityFrameworkInMemoryDatabase()
            //                 .BuildServiceProvider();

            return new DbContextOptionsBuilder<ApplicationDbContext>()
                 .UseInMemoryDatabase(databaseName)
                 .Options;

        }
    }
}
