﻿using YouWatchASP.Data.Models.Abstract;

namespace YouWatchASP.Data.Models
{
    public class ActorMovie
    {
        public int ActorId { get; set; }

        public int MovieId { get; set; }

        public Actor Actor { get; set; }

        public Movie Movie { get; set; }
    }
}
