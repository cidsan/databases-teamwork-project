﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using YouWatchASP.Data.Models.Abstract;

namespace YouWatchASP.Data.Models
{
    public class Director : BaseEntity
    {
        public Director()
        {
            this.MovieList = new List<Movie>();
        }

        [Required(ErrorMessage = "Please enter first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter last name.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter oscars count.")]
        public int Oscars { get; set; }

        [Url(ErrorMessage = "Invalid Url.")]
        public string ImageUrl { get; set; }

        public ICollection<Movie> MovieList { get; set; }
    }
}
