﻿using System;

namespace YouWatchASP.Data.Models.Exceptions
{
    public class ActorNotFoundException : Exception
    {
        public ActorNotFoundException(int id) : base($"Actor with Id: {id} was not found.")
        {
            ActorId = id;
        }

        public int ActorId { get; set; }
    }
}
