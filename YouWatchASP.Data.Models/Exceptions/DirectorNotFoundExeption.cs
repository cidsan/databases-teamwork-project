﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouWatchASP.Data.Models.Exceptions
{
    public class DirectorNotFoundExeption : Exception
    {
        public DirectorNotFoundExeption(int id) : base ($"Director with Id: {id} was not found.")
        {
            DirectorId = id;
        }

        public int DirectorId { get; set; }
    }
}
