﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouWatchASP.Data.Models.Exceptions
{
    public class MovieNotFoundException : Exception
    {
        public MovieNotFoundException(int id) : base($"Movie with Id: {id} was not found.")
        {
            MovieId = id;
        }

        public int MovieId { get; set; }
    }
}
