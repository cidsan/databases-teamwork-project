﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using YouWatchASP.Data.Models.Abstract;

namespace YouWatchASP.Data.Models
{
    public class Genre : BaseEntity
    {
        public Genre()
        {
            this.MovieGenres = new List<MovieGenre>();
        }

        [Required]
        public string Name { get; set; }

        public ICollection<MovieGenre> MovieGenres { get; set; }
    }
}
