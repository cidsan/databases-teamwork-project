﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using YouWatchASP.Data.Models.Abstract;

namespace YouWatchASP.Data.Models
{
    public class Keyword : BaseEntity
    {
        public Keyword()
        {
            this.MovieKeywords = new List<MovieKeyword>();
        }

        [Required]
        public string Word { get; set; }

        public ICollection<MovieKeyword> MovieKeywords { get; set; }
    }
}
