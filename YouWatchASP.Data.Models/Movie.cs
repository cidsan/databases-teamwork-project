﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using YouWatchASP.Data.Models.Abstract;

namespace YouWatchASP.Data.Models
{
    public class Movie : BaseEntity
    {

        public Movie()
        {
            this.ActorMovie = new List<ActorMovie>();
            this.MovieGenres = new List<MovieGenre>();
            this.MovieKeysWord = new List<MovieKeyword>();
            this.ProducerMovie = new List<ProducerMovie>();
        }

        

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int Oscars { get; set; }

        [Url(ErrorMessage ="Invalid url.")]
        public string TrailerUrl { get; set; }

        [Url(ErrorMessage = "Invalid url.")]
        public string ImageUrl { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public Director Director { get; set; }

        public int DirectorId { get; set; }

        public ICollection<ProducerMovie> ProducerMovie { get; set; }

        public ICollection<MovieKeyword> MovieKeysWord { get; set; }

        public ICollection<MovieGenre> MovieGenres { get; set; }

        public ICollection<ActorMovie> ActorMovie { get; set; }
        
    }
}
