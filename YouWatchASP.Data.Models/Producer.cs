﻿using System.Collections.Generic;
using YouWatchASP.Data.Models.Abstract;

namespace YouWatchASP.Data.Models
{
    public class Producer : BaseEntity
    {
        public Producer()
        {
            this.ProducerMovies = new List<ProducerMovie>();
        }

        public string Name { get; set; }

        public ICollection<ProducerMovie> ProducerMovies { get; set; }
    }
}
