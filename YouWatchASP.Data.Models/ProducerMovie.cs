﻿namespace YouWatchASP.Data.Models
{
    public class ProducerMovie
    {
        public int ProducerId { get; set; }

        public int MovieId { get; set; }

        public Movie Movie { get; set; }

        public Producer Producer { get; set; }
    }
}
