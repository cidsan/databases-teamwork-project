﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using YouWatchASP.Data.Models.Identity;

namespace CasinoTazdingo.Data.Configurations.Identity
{
    internal class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.HasIndex(u => u.Name).IsUnique(true);

            builder.HasIndex(u => u.Email).IsUnique(true);
        }
    }
}
