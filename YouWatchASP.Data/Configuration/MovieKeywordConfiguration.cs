﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Data.Configuration
{
    public class MovieKeywordConfiguration : IEntityTypeConfiguration<MovieKeyword>
    {
        public void Configure(EntityTypeBuilder<MovieKeyword> builder)
        {
            builder.HasKey(k => new { k.MovieId, k.KeywordId });
        }
    }
}
