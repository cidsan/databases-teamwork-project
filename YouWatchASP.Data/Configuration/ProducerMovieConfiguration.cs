﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Data.Configuration
{
    public class ProducerMovieConfiguration : IEntityTypeConfiguration<ProducerMovie>
    {
        public void Configure(EntityTypeBuilder<ProducerMovie> builder)
        {
            builder.HasKey(k => new { k.ProducerId, k.MovieId });
        }
    }
}
