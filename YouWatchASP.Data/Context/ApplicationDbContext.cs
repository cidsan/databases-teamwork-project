﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using YouWatchASP.Data.Models.Identity;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Configuration;

namespace YouWatchASP.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
        {

        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        //TODO: How to hide the connection string and why it doesn't work when I use it only in the StartUp?
       // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
       // {
       //     if (!optionsBuilder.IsConfigured)
       //     {
       //         optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=YouWatchASP;Trusted_Connection=True;");
       //     }
       //     
       // }

        public DbSet<Actor> Actors { get; set; }

        public DbSet<ActorMovie> ActorMovies { get; set; }

        public DbSet<Director> Directors { get; set; }

        public DbSet<Producer> Producers { get; set; }

        public DbSet<ProducerMovie> ProducerMovies { get; set; }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<MovieGenre> MovieGenres { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<Keyword> Keywords { get; set; }

        public DbSet<MovieKeyword> MovieKeyword { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            LoadJson.Load(builder);

            builder.ApplyConfiguration(new ActorMovieConfiguration());
            builder.ApplyConfiguration(new MovieGenreConfiguration());
            builder.ApplyConfiguration(new MovieKeywordConfiguration());
            builder.ApplyConfiguration(new ProducerMovieConfiguration());

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
