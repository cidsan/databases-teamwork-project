﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Data
{
    public static class LoadJson
    {
        public static void Load(ModelBuilder modelBuilder)
        {
            var actorsPath = @"..\YouWatchASP.Data\JsonImport\Actors.json";
            var genresPath = @"..\YouWatchASP.Data\JsonImport\Genres.json";
            var moviesPath = @"..\YouWatchASP.Data\JsonImport\Movies.json";
            var directorsPath = @"..\YouWatchASP.Data\JsonImport\Directors.json";
            var keyWordsPath = @"..\YouWatchASP.Data\JsonImport\Keywords.json";
            var producersPath = @"..\YouWatchASP.Data\JsonImport\Producers.json";
            var actorMoviePath = @"..\YouWatchASP.Data\JsonImport\ActorMovie.json";
            var movieGenrePath = @"..\YouWatchASP.Data\JsonImport\MovieGenre.json";
            var movieKeyWordPath = @"..\YouWatchASP.Data\JsonImport\MovieKeyword.json";
            var producerMoviePath = @"..\YouWatchASP.Data\JsonImport\ProducerMovie.json";

            try
            {
                var actorsJson = File.ReadAllText(actorsPath);
                var genresJson = File.ReadAllText(genresPath);
                var moviesJson = File.ReadAllText(moviesPath);
                var directorsJson = File.ReadAllText(directorsPath);
                var keyWordsJson = File.ReadAllText(keyWordsPath);
                var producersJson = File.ReadAllText(producersPath);
                var actorMovieJson = File.ReadAllText(actorMoviePath);
                var movieGenreJson = File.ReadAllText(movieGenrePath);
                var movieKeyWordJson = File.ReadAllText(movieKeyWordPath);
                var producerMovieJson = File.ReadAllText(producerMoviePath);

                var format = "dd/MM/yyyy"; // your datetime format
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                var actors = JsonConvert.DeserializeObject<Actor[]>(actorsJson, dateTimeConverter);
                var genres = JsonConvert.DeserializeObject<Genre[]>(genresJson, dateTimeConverter);
                var movies = JsonConvert.DeserializeObject<Movie[]>(moviesJson, dateTimeConverter);
                var directors = JsonConvert.DeserializeObject<Director[]>(directorsJson, dateTimeConverter);
                var keyWords = JsonConvert.DeserializeObject<Keyword[]>(keyWordsJson, dateTimeConverter);
                var producers = JsonConvert.DeserializeObject<Producer[]>(producersJson, dateTimeConverter);
                var actorMovie = JsonConvert.DeserializeObject<ActorMovie[]>(actorMovieJson, dateTimeConverter);
                var movieGenre = JsonConvert.DeserializeObject<MovieGenre[]>(movieGenreJson, dateTimeConverter);
                var movieKeyWord = JsonConvert.DeserializeObject<MovieKeyword[]>(movieKeyWordJson, dateTimeConverter);
                var producerMovie = JsonConvert.DeserializeObject<ProducerMovie[]>(producerMovieJson, dateTimeConverter);

                modelBuilder.Entity<Actor>().HasData(actors);
                modelBuilder.Entity<Genre>().HasData(genres);
                modelBuilder.Entity<Director>().HasData(directors);
                modelBuilder.Entity<Keyword>().HasData(keyWords);
                modelBuilder.Entity<Producer>().HasData(producers);
                modelBuilder.Entity<Movie>().HasData(movies);
                modelBuilder.Entity<ActorMovie>().HasData(actorMovie);
                modelBuilder.Entity<MovieGenre>().HasData(movieGenre);
                modelBuilder.Entity<MovieKeyword>().HasData(movieKeyWord);
                modelBuilder.Entity<ProducerMovie>().HasData(producerMovie);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
