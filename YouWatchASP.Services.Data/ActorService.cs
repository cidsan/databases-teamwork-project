﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Services.Data.Contracts;
using YouWatchASP.Services.Data.Utils;

namespace YouWatchASP.Services.Data
{
    public class ActorService : IActorService
    {
        private ApplicationDbContext _context;

        public ActorService(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Actor> CreateAsync(string firstName, string lastName, int oscars, string imageUrl)
        {
            Validator.ValidateNull(firstName, Constants.Empty_First_Name);
            Validator.ValidateNull(lastName, Constants.Empty_Last_Name);
            Validator.NegativeNumberCheck(oscars, Constants.Negative_Oscars);

            if (await _context.Actors.AnyAsync(a => a.FirstName == firstName && a.LastName == lastName))
            {
                throw new ArgumentException(Constants.Actor_Exists);
            }

            var newActor = new Actor()
            {
                FirstName = firstName,
                LastName = lastName,
                Oscars = oscars,
                ImageUrl = imageUrl,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now
            };

            await _context.Actors.AddAsync(newActor);
            await _context.SaveChangesAsync();

            return newActor;
        }

        public async Task<Actor> EditAsync(int id, string firstName, string lastName, int oscars, string imageUrl)
        {
            var actor = await _context.Actors.FirstOrDefaultAsync(a => a.Id == id);

            if(actor==null)
            {
                throw new ActorNotFoundException(id);
            }

            actor.FirstName = firstName;
            actor.LastName = lastName;
            actor.Oscars = oscars;
            actor.ImageUrl = imageUrl;
            actor.ModifiedOn = DateTime.Now;

            _context.Actors.Update(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        public async Task<Actor> AddMovieAsync(int actorId, int movieId)
        {
            var actor = await _context.Actors.FirstOrDefaultAsync(a => a.Id == actorId);

            if(actor==null)
            {
                throw new ActorNotFoundException(actorId);
            }

            var movie = await _context.Movies.FirstOrDefaultAsync(a => a.Id == movieId);

            if(movie==null)
            {
                throw new MovieNotFoundException(movieId);
            }

            var actorMovie = new ActorMovie
            {
                Actor = actor,
                Movie = movie,
                ActorId = actor.Id,
                MovieId = movie.Id
            };

            actor.ActorMovie.Add(actorMovie);
            await _context.SaveChangesAsync();

            return actor;
        }

        public async Task<Actor> RemoveAsync(int id)
        {
            var actor = await _context.Actors.FirstOrDefaultAsync(a => a.Id == id);

            if(actor==null)
            {
                throw new ActorNotFoundException(id);
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        public async Task<Actor> GetActorAsync(int id)
        {
            var actor = await _context.Actors
                .Include(a => a.ActorMovie)
                    .ThenInclude(am => am.Movie)
                .FirstOrDefaultAsync(a => a.Id == id);

            if (actor == null)
            {
                throw new ActorNotFoundException(id);
            }

            return actor;
        }

        public async Task<ICollection<Actor>> GetListOfActorsAsync()
        {
            var listOfActors = await _context.Actors.ToListAsync();

            return listOfActors;
        }

        public async Task<ICollection<Actor>> GetListOfActorsInMovieAsync(int id)
        {
            var listOfActors = await _context.Actors
                .Where(a => a.Id == id)
                .ToListAsync();

            return listOfActors;
        }
    }
}
