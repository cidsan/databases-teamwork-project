﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Services.Data.Contracts
{
    public interface IActorService
    {
        Task<Actor> CreateAsync(string firstName, string lastName, int oscars, string imageUrl);

        Task<Actor> EditAsync(int id, string firstName, string lastName, int oscars, string imageUrl);

        Task<Actor> RemoveAsync(int id);

        Task<Actor> GetActorAsync(int id);

        Task<ICollection<Actor>> GetListOfActorsAsync();

        Task<ICollection<Actor>> GetListOfActorsInMovieAsync(int id);

        Task<Actor> AddMovieAsync(int actorId, int movieId);
    }
}
