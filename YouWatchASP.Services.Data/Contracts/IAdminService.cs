﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Services.Data.Contracts
{
    public interface IAdminService
    {
        Task<Movie> CreateAsync(string name, string director, int oscars, string trailerUrl,
                                string imageUrl, string description, DateTime releaseDate);
    }
}
