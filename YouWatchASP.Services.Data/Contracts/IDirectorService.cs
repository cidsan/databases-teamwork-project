﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using X.PagedList;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Services.Data.Contracts
{
    public interface IDirectorService
    {
        Task<ICollection<Director>> GetListAsync();

        Task<IEnumerable<Director>> ListAllAsync(Expression<Func<Director, bool>> expression = null);

        Task<Director> CreateAsync(string firstName, string lastName, int oscars, string imageUrl);

        Task<Director> GetById(int id);

        Task<IPagedList<Director>> FilterDirectorsAsync(int pageSize = 9, int pageNumber = 1);

        Task<Director> EditAsync(int id, string firstName, string lastName, int oscars, string imageUrl);
    }
}
