﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Services.Data.Contracts
{
    public interface IMovieService 
    {
        Task<IReadOnlyCollection<Movie>> GetAllMovie();

        Task<string> FindMovieDirectorById(int Id);

        Task<Movie> GetMovieById(int id);

        Task<Movie> GetMovieByName(string name);

        Task<Movie> CreateAsync(string name, string director, int oscars, string trailerUrl, string imageUrl, string description, DateTime releaseDate);

        Task<IEnumerable<Movie>> ListAllAsync(Expression<Func<Movie, bool>> expression = null);
    }
}
