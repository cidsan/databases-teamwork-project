﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using X.PagedList;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data.Contracts;
using YouWatchASP.Services.Data.Utils;

namespace YouWatchASP.Services.Data
{
    public class DirectorService : IDirectorService
    {
        private ApplicationDbContext _context;

        public DirectorService(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IPagedList<Director>> FilterDirectorsAsync(int pageSize = 9, int pageNumber = 1)
        {
            var query = _context.Directors.Include(d => d.MovieList);

            return await query.ToPagedListAsync(pageNumber, pageSize);
        }

        public async Task<ICollection<Director>> GetListAsync()
        {
            var listOfDirectors = await _context.Directors
                                                .Include(d => d.MovieList)
                                                .OrderBy(d => d.FirstName).ToListAsync();


            return listOfDirectors;
        }

        public async Task<Director> GetById (int id)
        {
            var director = await _context.Directors.Include(d => d.MovieList).FirstOrDefaultAsync(d => d.Id == id);

            Validator.ValidateNull(director, Constants.Director_Doesnt_Exist);

            return director;
        }

        public async Task<IEnumerable<Director>> ListAllAsync(Expression<Func<Director, bool>> expression = null)
        {
            var output = await _context.Directors.Where(expression).ToListAsync();

            return output;
        }

        public async Task<Director> CreateAsync(string firstName, string lastName, int oscars, string imageUrl)
        {
            Validator.ValidateNull(firstName, Constants.Empty_First_Name);
            Validator.ValidateNull(lastName, Constants.Empty_Last_Name);

            if (await _context.Directors.AnyAsync(a => a.FirstName == firstName && a.LastName == lastName))
            {
                throw new ArgumentException(Constants.Director_Exists);
            }

            var director = new Director()
            {
                FirstName = firstName,
                LastName = lastName,
                Oscars = oscars,
                ImageUrl = imageUrl,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now
            };

            await _context.Directors.AddAsync(director);
            await _context.SaveChangesAsync();

            return director;
        }

        public async Task<Director> EditAsync(int id, string firstName, string lastName, int oscars, string imageUrl)
        {
            var director = await _context.Directors.FirstOrDefaultAsync(a => a.Id == id);

            director.FirstName = firstName;
            director.LastName = lastName;
            director.Oscars = oscars;
            director.ImageUrl = imageUrl ?? director.ImageUrl;
            director.ModifiedOn = DateTime.Now;

            _context.Directors.Update(director);
            await _context.SaveChangesAsync();

            return director;
        }
    }
}
