﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models;
using YouWatchASP.Services.Data.Contracts;
using YouWatchASP.Services.Data.Utils;

namespace YouWatchASP.Services.Data
{
    public class MovieService : IMovieService
    {
        private readonly ApplicationDbContext _dataBase;

        public MovieService(ApplicationDbContext dataBase)
        {
            _dataBase = dataBase ?? throw new ArgumentNullException(nameof(dataBase));
        }

        public async Task<Movie> CreateAsync(string name, string director, int oscars, string trailerUrl, string imageUrl, string description, DateTime releaseDate)
        {
            Validator.ValidateNull(name, Constants.Movie_Name_Cant_Be_Null);
            Validator.ValidateNull(director, Constants.Movie_Director_Cant_Be_Null);
            Validator.ValidateNull(trailerUrl, Constants.Movie_Trailer_Url_Cant_Be_Null);
            Validator.ValidateNull(imageUrl, Constants.Movie_Image_Url_Cant_Be_Null);

            if (await _dataBase.Movies.AnyAsync(m => m.Name == name))
            {
                throw new ArgumentException(Constants.Movie_Name_Exists);
            }

            var directorName = Validator.DirectorNameSpliter(director);

            if (directorName.Count < 2)
            {
                throw new ArgumentException(Constants.Director_Name_Split_Error);
            }

            var directorExist = await _dataBase.Directors.FirstOrDefaultAsync(d => d.FirstName == directorName[0] && d.LastName == directorName[1]);

            if (directorExist == null)
            {
                throw new ArgumentException(Constants.Dorector_Dosent_Exist);
            }

            Movie movie = new Movie()
            {
                Name = name,
                Director = directorExist,
                DirectorId = directorExist.Id,
                Oscars = oscars,
                TrailerUrl = trailerUrl,
                ImageUrl = imageUrl,
                Description = description,
                ReleaseDate = releaseDate,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now

            };

            await _dataBase.Movies.AddAsync(movie);
            await _dataBase.SaveChangesAsync();

            return movie;

        }

        public async Task<IReadOnlyCollection<Movie>> GetAllMovie()
        {
            var movieList = await _dataBase.Movies.Include(d => d.Director).ToListAsync();

            Validator.ZeroCountValidate(movieList, Constants.MovieList_Is_Empty);

            return movieList;
        }
        public async Task<IEnumerable<Movie>> ListAllAsync(Expression<Func<Movie, bool>> expression = null)
        {
            var output = await _dataBase.Movies.Where(expression).ToListAsync();

            return output;
        }


        public async Task<string> FindMovieDirectorById(int Id)
        {
            var director = await _dataBase.Directors.FirstOrDefaultAsync(d => d.Id == Id);

            Validator.ValidateNull(director, Constants.Director_cant_Be_Null);

            return string.Join(' ', director.FirstName, director.LastName);
        }

        public async Task<Movie> GetMovieById(int id)
        {
            Movie movie = await _dataBase.Movies
                                   .Include(am => am.ActorMovie)
                                   .ThenInclude(a => a.Actor)
                                   .Include(mg => mg.MovieGenres)
                                   .ThenInclude(g => g.Genre)
                                   .Include(pm => pm.ProducerMovie)
                                   .ThenInclude(p => p.Producer)
                                   .Include(mk => mk.MovieKeysWord)
                                   .ThenInclude(k => k.Keyword)
                                   .Include(d => d.Director).FirstOrDefaultAsync(m => m.Id == id);

            Validator.ValidateNull(movie, Constants.Movie_Cant_Be_Null);

            return movie;
        }
        public async Task<Movie> GetMovieByName(string name)
        {
            Movie movie = await _dataBase.Movies
                                   .Include(am => am.ActorMovie)
                                   .ThenInclude(a => a.Actor)
                                   .Include(mg => mg.MovieGenres)
                                   .ThenInclude(g => g.Genre)
                                   .Include(pm => pm.ProducerMovie)
                                   .ThenInclude(p => p.Producer)
                                   .Include(mk => mk.MovieKeysWord)
                                   .ThenInclude(k => k.Keyword)
                                   .Include(d => d.Director).FirstOrDefaultAsync(m => m.Name == name);

            Validator.ValidateNull(movie, Constants.Movie_Cant_Be_Null);

            return movie;
        }

        public async Task<IList<Director>> FindAllMovieWithDirector(string name)
        {
            var directorName = Validator.DirectorNameSpliter(name);

            if (directorName.Count < 2)
            {
                throw new ArgumentException(Constants.Director_Name_Split_Error);
            }

            var movieList = await _dataBase.Directors.Include(md => md.MovieList)
                
                                  .Where(d => d.FirstName == directorName[0] && d.LastName == directorName[1]).ToListAsync();

          //  Validator.ValidateNull(director, Constants.Director_cant_Be_Null);

            return movieList;
        }
    }
}
