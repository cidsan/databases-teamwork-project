﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouWatchASP.Services.Data.Utils
{
    internal class Constants
    {
        //Actor messages
        internal const string Empty_First_Name = "First name cannot be empty.";
        internal const string Empty_Last_Name = "Last name cannot be empty.";
        internal const string Empty_Oscars = "Oscars cannot be empty.";
        internal const string Negative_Oscars = "Oscars cannot be negative.";
        internal const string Actor_Exists = "Error, actor with the same names already exists.";
        internal const string Actor_Doesnt_Exist = "The requested actor doesn't exist in the database.";
        internal const string Movie_Cant_Be_Null = "The requested movie dosen't exist in the database.";
        internal const string Director_cant_Be_Null = "The requested director dosen't exist in the database.";
        internal const string Director_Exists = "Error, director with the same names already exists.";
        internal const string Director_Doesnt_Exist = "The requested director doesn't exist in the database.";
        internal const string MovieList_Is_Empty = "The requested movie list is empty.";

        internal const string Movie_Name_Cant_Be_Null = "Movie name cannot be null.";
        internal const string Movie_Director_Cant_Be_Null = "Director name cannot be null.";
        internal const string Movie_Trailer_Url_Cant_Be_Null = "Trailer url cannot be null.";
        internal const string Movie_Image_Url_Cant_Be_Null = "Image url cannot be null.";
        internal const string Movie_Name_Exists = "Error, movie with the same name already exist.";
        internal const string Dorector_Dosent_Exist = "Director with that name dosen't exist in the database.";
        internal const string Director_Name_Split_Error = "Invalid director name input, correct form must be \"FirstName LastName\"";
        internal const string Actor_Not_Found = "The requested actor was not found.";
    }
}
