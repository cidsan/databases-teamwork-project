﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YouWatchASP.Services.Data.Utils
{
    internal class Validator
    {
        internal static void ValidateNull(object name, string message)
        {
            if(name==null)
            {
                throw new ArgumentNullException(message);
            }
        }

        internal static void ZeroCountValidate(IReadOnlyCollection<object> name, string message)
        {
            if(name.Count == 0)
            {
                throw new ArgumentException(message);
            }
        }

        internal static void NegativeNumberCheck(int number, string message)
        {
            if(number < 0)
            {
                throw new ArgumentOutOfRangeException(message);
            }
        }

        internal static IList<string> DirectorNameSpliter(string input)
        {
            var listName = input.Split().ToList();

            return listName;
        }
    }
}
