﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Areas.Administration.Models.ActorsAdminViewModels;
using YouWatchASP.Areas.Utils;
using YouWatchASP.Services.Data.Contracts;

namespace YouWatchASP.Areas.Administration.Controllers
{
    [Area("Administration")]
    [Authorize(Roles = "Administrator")]
    [Route("[area]/[controller]/[action]")]
    public class ActorsAdminController : Controller
    {
        private readonly IActorService _actorService;
        private readonly IMapper _mapper;

        public ActorsAdminController(IActorService actorService, IMapper mapper)
        {
            _actorService = actorService ?? throw new ArgumentNullException(nameof(actorService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public async Task<IActionResult> Index(
            string sortOrder,
            string searchString,
            string currentFilter,
            int? pageNumber
            )
        {

            ViewData["FirstNameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "FirstName_Desc" : "";
            ViewData["LastNameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "LastName_Desc" : "";
            ViewData["OscarsSortParam"] = String.IsNullOrEmpty(sortOrder) ? "Oscars_Desc" : "";
            ViewData["ModifiedDateSortParam"] = String.IsNullOrEmpty(sortOrder) ? "ModifiedDate_Desc" : "ModifiedDate";
            ViewData["CreatedDateSortParam"] = sortOrder == "CreatedDate" ? "CreatedDate_Desc" : "CreatedDate";
            ViewData["CurrentSort"] = sortOrder;

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var actors = await _actorService.GetListOfActorsAsync();

            if (!String.IsNullOrEmpty(searchString))
            {
                actors = actors.Where(a => a.FirstName.ToLower().Contains(searchString.ToLower())
                                       || a.LastName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "FirstName_Desc":
                    actors = actors.OrderByDescending(a => a.FirstName).ToList();
                    break;
                case "LastName_Desc":
                    actors = actors.OrderByDescending(a => a.LastName).ToList();
                    break;
                case "Oscars_Desc":
                    actors = actors.OrderByDescending(a => a.Oscars).ToList();
                    break;
                case "CreatedDate":
                    actors = actors.OrderBy(a => a.CreatedOn).ToList();
                    break;
                case "CreatedDate_Desc":
                    actors = actors.OrderByDescending(a => a.CreatedOn).ToList();
                    break;
                case "ModifiedDate":
                    actors = actors.OrderBy(a => a.ModifiedOn).ToList();
                    break;
                case "ModifiedDate_Desc":
                    actors = actors.OrderByDescending(a => a.ModifiedOn).ToList();
                    break;
                default:
                    actors = actors.OrderBy(a => a.FirstName)
                        .ThenBy(a => a.LastName).ToList();
                    break;
            }

            IEnumerable<ActorAdminViewModel> vm = actors.Select(_mapper.Map<ActorAdminViewModel>).ToList();

            int pageSize = 7;   

            return View(await PaginatedList<ActorAdminViewModel>.CreateAsync(vm, pageNumber ?? 1, pageSize));
        }


    }
}