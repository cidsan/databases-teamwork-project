﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models.Common;
using YouWatchASP.Data.Models.Identity;

namespace YouWatchASP.Configuration
{
    public class Seeder
    {
        public static async Task SeedDataAsync(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            await SeedRolesAsync(roleManager);
            await SeedUsersAsync(userManager);
        }

        public static async Task SeedRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            if(!await roleManager.RoleExistsAsync(RolesType.Administrator.ToString()))   
            {
                IdentityRole newRole = new IdentityRole() { Name = RolesType.Administrator.ToString() };
                await roleManager.CreateAsync(newRole);
            }

            if (!await roleManager.RoleExistsAsync(RolesType.User.ToString())) 
            {
                IdentityRole newRole = new IdentityRole() { Name = RolesType.User.ToString() };
                await roleManager.CreateAsync(newRole);
            }
        }

        public static async Task SeedUsersAsync(
            UserManager<ApplicationUser> userManager)
        {
            if (!await userManager.Users.AnyAsync(u => u.UserName == RolesType.Administrator.ToString()))
            {
               
                ApplicationUser newUser = new ApplicationUser()
                {
                    
                    UserName ="Administrator",
                    Email = "admin@asd.bg",
                    
                };

                if ((await userManager.CreateAsync(newUser,"P@sw0rd")).Succeeded)
                {
                    await userManager.AddToRoleAsync(newUser, RolesType.Administrator.ToString());
                }
            }
        }
    }
}
