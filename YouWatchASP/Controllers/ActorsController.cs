﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Models;
using YouWatchASP.Services.Data.Contracts;

namespace YouWatchASP.Controllers
{
    [Authorize(Roles = "Administrator,User")]
    public class ActorsController : Controller
    {
        private readonly IActorService _actorService;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;
        private readonly IMemoryCache _cache;
        private const string ActorsCacheKey = "actors";

            public ActorsController(IActorService actorService, IMapper mapper, IMovieService movieService, IMemoryCache cache)
        {
            _actorService = actorService ?? throw new ArgumentNullException(nameof(actorService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _movieService = movieService ?? throw new ArgumentNullException(nameof(movieService));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        [HttpGet]
        public async Task<IActionResult> Index(string searchString)
        {
            ICollection<ActorViewModel> cachedActors;

            if(!_cache.TryGetValue(ActorsCacheKey, out cachedActors))
            {

                ICollection<Actor> actors = await _actorService.GetListOfActorsAsync();
                var orderedActors = actors.OrderBy(a => a.FirstName)
                        .ThenBy(a => a.LastName);
                cachedActors = orderedActors.Select(_mapper.Map<ActorViewModel>).ToList();

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                                        .SetSlidingExpiration(TimeSpan.FromSeconds(60));

                _cache.Set(ActorsCacheKey, cachedActors, cacheEntryOptions);
            }

            if (!String.IsNullOrEmpty(searchString))
            {
                cachedActors = cachedActors.Where(a => a.FirstName.ToLower().Contains(searchString.ToLower())
                               || a.LastName.ToLower().Contains(searchString.ToLower())).ToList();
                cachedActors = cachedActors.Select(_mapper.Map<ActorViewModel>).ToList();
            }

            return View(cachedActors);


        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            ActorViewModel cachedActor;

            try
            {
                var singleActorKey = ActorsCacheKey + id.ToString();

                if (!_cache.TryGetValue(singleActorKey, out cachedActor))
                {
                    var actor = await _actorService.GetActorAsync(id);
                    cachedActor = _mapper.Map<ActorViewModel>(actor);

                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                                        .SetSlidingExpiration(TimeSpan.FromSeconds(60));

                    _cache.Set(singleActorKey, cachedActor, cacheEntryOptions);
                }

                return View(cachedActor);
            }
            catch (ActorNotFoundException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return NotFound();
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Remove(ActorViewModel model)
        {
            try
            {
                var actor = await _actorService.RemoveAsync(model.Id);

                TempData["SuccessRemoved"] = $"{actor.FirstName} {actor.LastName} has been successfully removed from the database.";
                return RedirectToAction(nameof(Index));
            }
            catch(ActorNotFoundException)
            {
                return NotFound();
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(model);
            }
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var actor = await _actorService.GetActorAsync(id);

                var actorViewModel = _mapper.Map<ActorViewModel>(actor);

                return View(actorViewModel);
            }
            catch(ActorNotFoundException)
            {
                return NotFound();
            }
            catch(ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return View(id);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ActorViewModel actorViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View(actorViewModel);
            }

            try
            {
                var actor = await _actorService.EditAsync(actorViewModel.Id,
                    actorViewModel.FirstName, actorViewModel.LastName, actorViewModel.Oscars, actorViewModel.ImageUrl);

                TempData["Success"] = "Actor successfully updated.";
                return RedirectToAction(nameof(Details), new { id = actor.Id });
            }
            catch (ActorNotFoundException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return NotFound();
            }
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ActorViewModel actorViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View(actorViewModel);
            }

            try
            {
                var actor = await _actorService.CreateAsync(
                    actorViewModel.FirstName, actorViewModel.LastName, actorViewModel.Oscars, actorViewModel.ImageUrl);
                return RedirectToAction(nameof(Details), new { id = actor.Id });

            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                TempData["Error"] = "Actor with that name already exists.";
                return View(actorViewModel);
            }

        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AddMovie(int id)
        {
            var actor = await _actorService.GetActorAsync(id);

            var actorMovieViewModel = _mapper.Map<ActorMovieViewModel>(actor);

            var allMovies = await _movieService.GetAllMovie();

            var availableMovies = allMovies.Except(actor.ActorMovie.Select(x => x.Movie)).ToList();

            actorMovieViewModel.AvailableMovies = availableMovies.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToList();

            return View(actorMovieViewModel);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMovie(ActorMovieViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                var actor = await _actorService.AddMovieAsync(model.ActorId, model.MovieId);
                return RedirectToAction(nameof(Details), new { id = model.ActorId });

            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                TempData["Error"] = "Actor with that name already exists.";
                return View(model);
            }

        }
    }
}
