﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Models;
using YouWatchASP.Services.Data.Contracts;
using YouWatchASP.Utils;

namespace YouWatchASP.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAdminService _adminService;
        private readonly IDirectorService _directorService;

        public AdminController(IAdminService adminService, IDirectorService directorService)
        {
            _adminService = adminService ?? throw new ArgumentNullException(nameof(adminService));
            _directorService = directorService;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        
        public async Task<IActionResult> Create(MovieViewModel movieViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(movieViewModel);
            }

            try
            {
              var movie = await  _adminService.CreateAsync(movieViewModel.Name, movieViewModel.DirectorName,
                                          movieViewModel.Oscars,
                                          movieViewModel.TrailerUrl, movieViewModel.ImageUrl,
                                          movieViewModel.Description, movieViewModel.ReleaseDate);
               
              return  RedirectToAction("Details", "Movies", new { id = movie.Id });

               
            }
            catch (Exception ex)
            {

                this.ModelState.AddModelError("Error", ex.Message);
                return View();
            }
        }


        [HttpGet("Admin/AutoComplete/{term}")]
        public async Task<JsonResult> AutoComplete([FromRoute]string term)
        {
            IEnumerable<string> result = (await _directorService
                .ListAllAsync(d => d.GetDirectorFullName().ToLower().Contains(term)))
                .Select(d => d.GetDirectorFullName());

            return Json(result);
        }
    }
}