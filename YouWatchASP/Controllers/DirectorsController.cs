﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using YouWatchASP.Data.Models;
using YouWatchASP.Data.Models.Exceptions;
using YouWatchASP.Models;
using YouWatchASP.Models.Director;
using YouWatchASP.Services.Data;
using YouWatchASP.Services.Data.Contracts;

namespace YouWatchASP.Controllers
{
    [Authorize(Roles = "Administrator,User")]
    public class DirectorsController : Controller
    {
        private readonly IDirectorService _directorService;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;
        private const string DirectorsCacheKey = "Director";

        public DirectorsController(IDirectorService directorService, IMapper mapper, IMemoryCache cache)
        {
            _directorService = directorService ?? throw new ArgumentNullException(nameof(directorService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        public async Task<IActionResult> Index()
        {
            ICollection<DirectorViewModel> cachedDirectors;

            if(!_cache.TryGetValue(DirectorsCacheKey,out cachedDirectors))
            {
                var directorList = await _directorService.GetListAsync();


                cachedDirectors = directorList.Select(_mapper.Map<DirectorViewModel>)
                                                                .OrderBy(m => m.FirstName).ToList();

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                                        .SetSlidingExpiration(TimeSpan.FromSeconds(60));

                _cache.Set(DirectorsCacheKey, cachedDirectors, cacheEntryOptions);
            }
           
            return View(cachedDirectors);
            
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var director = await _directorService.GetById(id);

            return  View(_mapper.Map<DirectorViewModel>(director));
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create (DirectorViewModel directorViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                var director =await  _directorService.CreateAsync(directorViewModel.FirstName, directorViewModel.LastName,
                                                            directorViewModel.Oscars, directorViewModel.ImageUrl);

                return RedirectToAction("Details", new { id = director.Id });
            }
            catch (ArgumentException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                TempData["Error"] = "Director with that name already exists.";
                return View();
            }
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var director = await _directorService.GetById(id);

                var directorViewModel = _mapper.Map<DirectorViewModel>(director);

                return View(directorViewModel);
            }
            catch (DirectorNotFoundExeption)
            {
                return NotFound();
            }
            
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(DirectorViewModel directorViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return View(directorViewModel);
            }

            try
            {
                var director = await _directorService.EditAsync(directorViewModel.Id,
                    directorViewModel.FirstName, directorViewModel.LastName, directorViewModel.Oscars, directorViewModel.ImageUrl);

                TempData["Success"] = "Actor successfully updated.";
                return RedirectToAction(nameof(Details), new { id = director.Id });
            }
            catch (ActorNotFoundException ex)
            {
                this.ModelState.AddModelError("Error", ex.Message);
                return NotFound();
            }
        }
    }
}