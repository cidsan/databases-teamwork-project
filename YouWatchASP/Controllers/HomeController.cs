﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using YouWatchASP.Models;
using YouWatchASP.Services.Data.Contracts;

namespace YouWatchASP.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IActorService _actorService;
        

        public HomeController(IActorService actorService)
        {
            _actorService = actorService ?? throw new ArgumentNullException(nameof(actorService));
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [Route("Home/Error/{statusCode}")]
        public IActionResult Error(int statusCode)
        {
            //if (statusCode == 404)
            //{
            //    var statusFeature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            //}

            return View(statusCode);

            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
