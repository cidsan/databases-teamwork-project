﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Models;
using YouWatchASP.Models.Director;
using YouWatchASP.Services.Data.Contracts;
using YouWatchASP.Utils;

namespace YouWatchASP.Controllers
{
    [Authorize(Roles = "Administrator,User")]
    public class MoviesController : Controller
    {
        private readonly IDirectorService _directorService;
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IDirectorService directorService,
                               IMovieService movieService,
                               IMapper mapper)
        {
            _directorService = directorService;
            _movieService = movieService;
            _mapper = mapper;
        }
        
        
        public async Task<IActionResult> Index()
        {

            var  movieList = await _movieService.GetAllMovie();
            ICollection<MovieViewModel> vm = movieList.Select(_mapper.Map<MovieViewModel>)
                                       .OrderBy(m => m.Name).ToList();
            return View(vm);
        }
        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            var movie = await _movieService.GetMovieById(id);
            MovieViewModel vm = _mapper.Map<MovieViewModel>(movie);
            return View(vm);
        }

        [HttpGet]
        public IActionResult FilterByDirectorName()
        {
            
            return View();
        }


        [HttpGet]
        public IActionResult Search([FromQuery]SearchByName model)
        {
            if (string.IsNullOrWhiteSpace(model.SearchName) ||
                model.SearchName.Length < 3)
            {
                return View();
            }

           // model.SearchResults = this.playerService.Search(model.SearchName)
           //                                         .Select(this.playerMapper.MapFrom)
           //                                         .ToList();

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MovieViewModel movieViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(movieViewModel);
            }

            try
            {
                var movie = await _movieService.CreateAsync(movieViewModel.Name, movieViewModel.DirectorName,
                                            movieViewModel.Oscars,
                                            movieViewModel.TrailerUrl, movieViewModel.ImageUrl,
                                            movieViewModel.Description, movieViewModel.ReleaseDate);

                return RedirectToAction("Details", "Movies", new { id = movie.Id });


            }
            catch (Exception ex)
            {

                this.ModelState.AddModelError("Error", ex.Message);
                return View();
            }
        }

        [HttpGet("Movies/AutoComplete/{term}")]
        public async Task<JsonResult> AutoComplete([FromRoute]string term)
        {
            IEnumerable<string> result = (await _directorService
                .ListAllAsync(d => d.GetDirectorFullName().ToLower().Contains(term)))
                .Select(d => d.GetDirectorFullName());

            return Json(result);
        }


        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult Update()
        {

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Update(MovieViewModel movieViewModel)
        {

            try
            {
                var movie = await _movieService.GetMovieByName(movieViewModel.Name);


                if (movie != null)
                {
                   
                    return RedirectToAction("Details", new { id = movie.Id });

                }
                else
                {
                    return View(movieViewModel);
                }

            }
            catch (Exception ex)
            {


                this.ModelState.AddModelError("Error", ex.Message);
                return View();
            }
            
        }

        [HttpGet("Movies/AutoCompleteMovie/{term}")]
        public async Task<JsonResult> AutoCompleteMovie([FromRoute]string term)
        {
            IEnumerable<string> result = (await _movieService
                .ListAllAsync(m => m.Name.ToLower().Contains(term)))
                .Select(m => m.Name);
                

            return Json(result);
        }
    }
}
