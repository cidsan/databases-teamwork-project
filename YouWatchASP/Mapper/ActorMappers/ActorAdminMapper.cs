﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Areas.Administration.Models.ActorsAdminViewModels;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Mapper.ActorMappers
{
    public class ActorAdminMapper : Profile
    {
        public ActorAdminMapper()
        {
            CreateMap<Actor, ActorAdminViewModel>()
                 .ForMember(a => a.ActorMovies, src => src.MapFrom(a => a.ActorMovie))
                 .ForMember(a => a.Id, src => src.MapFrom(a => a.Id))
                 .ForMember(a => a.FirstName, src => src.MapFrom(a => a.FirstName))
                 .ForMember(a => a.LastName, src => src.MapFrom(a => a.LastName))
                 .ForMember(a => a.Oscars, src => src.MapFrom(a => a.Oscars))
                 .ForMember(a => a.ImageUrl, src => src.MapFrom(a => a.ImageUrl))
                 .ForMember(a => a.CreatedOn, src => src.MapFrom(a => a.CreatedOn))
                 .ForMember(a => a.ModifiedOn, src => src.MapFrom(a => a.ModifiedOn));
        }
    }
}
