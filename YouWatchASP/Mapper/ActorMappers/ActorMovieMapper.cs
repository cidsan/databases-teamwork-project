﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;
using YouWatchASP.Models;

namespace YouWatchASP.Mapper.ActorMappers
{
    public class ActorMovieMapper : Profile
    {
        public ActorMovieMapper()
        {
            CreateMap<Actor, ActorMovieViewModel>()
                 .ForMember(a => a.ActorId, src => src.MapFrom(a => a.Id))
                 .ForMember(a => a.FirstName, src => src.MapFrom(a => a.FirstName))
                 .ForMember(a => a.LastName, src => src.MapFrom(a => a.LastName));
        }
    }
}
