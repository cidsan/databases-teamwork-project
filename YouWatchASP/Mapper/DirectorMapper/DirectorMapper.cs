﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;
using YouWatchASP.Models.Director;

namespace YouWatchASP.Mapper.DirectorMapper
{
    public class DirectorMapper : Profile
    {
        public DirectorMapper()
        {
            CreateMap<Director, DirectorViewModel>()
               .ForMember(m => m.MovieCollection, src => src.MapFrom(dm => dm.MovieList
                                                            .Select(d => d.Name)));
                
        }
    }
}
