﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;
using YouWatchASP.Models;

namespace YouWatchASP.Mapper.MovieMappers
{
    public class MovieMapper : Profile
    {
        public MovieMapper()
        {
            CreateMap<Movie, MovieViewModel>()
                 .ForMember(a => a.Actors, src => src.MapFrom(am => am.ActorMovie
                                                     .Select(ac => ac.Actor.FirstName + " " + ac.Actor.LastName)))
                 .ForMember(p => p.Producers, src => src.MapFrom(pm => pm.ProducerMovie
                                                     .Select(p => p.Producer.Name)))
                 .ForMember(g => g.Genres, src => src.MapFrom(mg => mg.MovieGenres
                                                         .Select(g => g.Genre.Name)))
                 .ForMember(k => k.KeyWords, src => src.MapFrom(mk => mk.MovieKeysWord
                                                         .Select(k => k.Keyword.Word)))
                 .ForMember(d => d.DirectorName, src => src.MapFrom
                                                        (dr => dr.Director.FirstName + " " + dr.Director.LastName));

        }
    }
}
