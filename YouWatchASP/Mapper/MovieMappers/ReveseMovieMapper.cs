﻿using AutoMapper;
using YouWatchASP.Data.Models;
using YouWatchASP.Models;

namespace YouWatchASP.Mapper.MovieMappers
{
    public class ReveseMovieMapper : Profile
    {
        public ReveseMovieMapper()
        {
            CreateMap<MovieViewModel, Movie>();
        }

    }
}
