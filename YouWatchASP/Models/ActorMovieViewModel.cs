﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouWatchASP.Models
{
    public class ActorMovieViewModel
    {
        public ActorMovieViewModel()
        {
            AvailableMovies = new List<SelectListItem>();
        }

        public int ActorId { get; set; }

        public int MovieId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<SelectListItem> AvailableMovies { get; set; }

    }
}
