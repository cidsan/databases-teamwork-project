﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Models
{
    public class ActorViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage ="Please enter a last name.")]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The last name can contain only letter")]
        public string LastName { get; set; }

        [Range(0,100)]
        [Required(ErrorMessage ="Please enter oscars count.")]
        public int Oscars { get; set; }

        public ICollection<ActorMovie> ActorMovies { get; set; }

        public string ImageUrl { get; set; }
    }
}
