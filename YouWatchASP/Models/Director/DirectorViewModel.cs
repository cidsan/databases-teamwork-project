﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace YouWatchASP.Models.Director
{
    public class DirectorViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter last name.")]
        public string  LastName { get; set; }

        [Required(ErrorMessage = "Please enter oscars count.")]
        public int Oscars { get; set; }

        [Url(ErrorMessage = "Invalid Url.")]
        public string ImageUrl { get; set; }

        public ICollection<string> MovieCollection;


    }
}
