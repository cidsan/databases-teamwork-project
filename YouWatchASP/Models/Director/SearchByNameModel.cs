﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Models.Director
{
    public class SearchByName
    {
        public string SearchName { get; set; }

        public IReadOnlyList<MovieViewModel> SearchResults { get; set; } = new List<MovieViewModel>();
    }
}
