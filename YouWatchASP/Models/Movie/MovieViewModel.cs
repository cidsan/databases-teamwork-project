﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using YouWatchASP.Data.Models;

namespace YouWatchASP.Models
{
    public class MovieViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string Name { get; set; }

        public int Oscars { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string DirectorName { get; set; }

        public string Description { get; set; }


        [Required]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        [Url(ErrorMessage = "Invalid url.")]
        public string TrailerUrl { get; set; }

        [Url(ErrorMessage = "Invalid url.")]
        public string ImageUrl { get; set; }

        public ICollection<string> Actors { get; set; }

        public ICollection<string> Genres { get; set; }

        public  ICollection<string> Producers { get; set; }

        public ICollection<string> KeyWords { get; set; }
    }
}
