﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using YouWatchASP.Configuration;
using YouWatchASP.Data;
using YouWatchASP.Data.Context;
using YouWatchASP.Data.Models.Identity;
using YouWatchASP.Services.Data.Contracts;

namespace YouWatchASP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            DataSeed(host).GetAwaiter().GetResult();

            ApplyMigration(host).GetAwaiter().GetResult();

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        public static async Task ApplyMigration(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var _applicationDbContext = services.GetRequiredService<ApplicationDbContext>();
                    await _applicationDbContext.Database.MigrateAsync();
                }
                catch (Exception ex)
                {
                    var _logger = services.GetRequiredService<ILogger<Program>>();
                    _logger.LogError(ex, "An error occured while trying to apply migrations.");
                }
            }
        }
        public static async Task DataSeed(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var _userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                    var _roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

                    services.GetRequiredService<ApplicationDbContext>().Database.EnsureCreated();
                    
                    await Seeder.SeedDataAsync(
                        userManager: _userManager,
                        roleManager: _roleManager);
                }
                catch (Exception ex)
                {
                    var _logger = services.GetRequiredService<ILogger<Program>>();
                    _logger.LogError(ex, "An error occured while seeding the database.");
                }
            }
        }
    }
}
