﻿using YouWatchASP.Data.Models;

namespace YouWatchASP.Utils
{
    public static class DomainModelsExtentions
    {
        public static string GetDirectorFullName(this Director director)
        {
            return $"{director.FirstName} {director.LastName}";
        }
    }
}
