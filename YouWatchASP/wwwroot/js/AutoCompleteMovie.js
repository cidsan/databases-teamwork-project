﻿$(document).ready(function () {
    $("#Name").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "AutoCompleteMovie/" + request.term,
                type: "GET",
                contentType: 'application/json',
                dataType: "json",
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item, value: item };
                    }))
                }
            })
        }
    });
});